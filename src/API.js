
export function updateApi(url, option) {
    return new Promise(async (resolve, reject)=>{
      let updateUrl = url + 'polls/P01/options/' + option;
          try{
              const response = await fetch(updateUrl,         
                {
                  method: 'PATCH'
                })   
                  if (response.ok) {
                    const responseObj = await response.json();
                    resolve(responseObj)
                  } else {
                    throw new Error('error');
                  }
              } catch (e) {
                  console.log('error');
                  reject(e); 
              }
        
        })
  }
 



