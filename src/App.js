import React, { Component } from 'react';
import './App.css';
import { updateApi } from './API';


export default class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      poll: [],
      url: ""
    }
    this.handleChange = this.handleChange.bind(this);
   }

  async componentDidMount() {
    this.setState({
      url : "https://3ecx3vkcv7.execute-api.ap-east-1.amazonaws.com/dev/"
    })
  }

  handleChange(event) {    
    this.setState({url: event.target.value});  
    console.log(this.state.url)
  }

  async updatePoll(url, option) {
    console.log(url)
    try {
      const poll = await updateApi(url, option); 
      let pollValues = Object.values(poll);
      alert('COUNT: ' + pollValues[1]); 
    } catch (error) {
      console.log('error');
    }
  }

  render() {
  const { url } = this.state;
  return (
    <div className="App">
      <header className="App-header">

        <div className="Heading-Container">
        <h1 className="heading">QUESTION</h1>
        <h3 className="heading1">WHICH IS YOUR FAVOURITE</h3>
        <h3 className="heading2">PROGRAMMING LANGUAGE?</h3>
        </div>

        <div className="Heading-Container-padding"></div>

        <div className="Button-Container">
        <button className="Button" size="large" onClick={()=>{this.updatePoll(url, 1)}}>
          1). Node.js
        </button>
        </div>
        
        <div className="Button-Container">
        <button className="Button" onClick={()=>{this.updatePoll(url, 2)}}>
          2). Python
        </button>
        </div>

        <div className="Button-Container">
        <button className="Button" onClick={()=>{this.updatePoll(url, 3)}}>
         3). Java
        </button>
        </div>

        <div className="Button-Container">
        <button className="Button" onClick={()=>{this.updatePoll(url, 4)}}>
         4). C#
        </button>
        </div>

        <form>
        <div className="form">
          <div className="api">
            API HOST: 
          </div>
          <input type="text" className="form-control" value={url} onChange={this.handleChange} />
        </div>
        </form>
         
      </header>
    </div>
  );
 }
}